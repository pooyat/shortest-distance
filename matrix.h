/* ===================================================================
 * Matrix:  Square Matrix class of n * n dimensions
 * Pooya Taherkhani,  pt375611 @ ohio . edu
 * =================================================================== */

#ifndef MATRIX_H
#define MATRIX_H

class Matrix {
 private:
  unsigned rows, cols;		// matrix dimensions,  unsigned int
  unsigned* int_ptr;		// matrix of distances,  unsigned int

 public:
  Matrix( unsigned n_dim, unsigned m_dim); // Constructor
  unsigned& operator() (unsigned row, unsigned col);
  unsigned operator() (unsigned row, unsigned col) const;
  bool operator ==(const Matrix& other);

  // the BIG three!
  ~Matrix();				   // Destructor
  Matrix( const Matrix& other);		   // Copy Constructor
  Matrix& operator =(const Matrix& other); // Assignment Operator
};

#endif
